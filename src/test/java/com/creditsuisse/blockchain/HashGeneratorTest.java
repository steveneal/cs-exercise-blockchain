package com.creditsuisse.blockchain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class HashGeneratorTest {

    @Test
    public void checkSameHashReturnedForSameInput() {
        //TODO: encode a String twice and ensure the hash results are THE SAME for each encoding
    }

    @Test
    public void checkDifferentHashReturnedForDifferentInput() {
        //TODO: encode two different String values and ensure the hash results are DIFFERENT for each encoding
    }

}
